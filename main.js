//Fonction qui supprime tout le content
function clearContent(){
    
    var nodeList = document.getElementById("content");
    
    while (nodeList.firstChild) {
        nodeList.removeChild(nodeList.firstChild);
    }
    
}

//Fonction creation d'un Div
function addDiv(){

    var newDiv = document.createElement("div");
    
    newDiv.className = "testDiv";
    
    document.getElementById("content").appendChild(newDiv);
    
}

//Fonction test2
function test2(){
    
     var newDiv = document.createElement("div");
    
    newDiv.className = "testDiv";
    
    newDiv.innerHTML = "<p>Bonjour</p>";
    
    document.getElementById("content").appendChild(newDiv);
    
    document.getElementsByTagName("p")[0].style.color = "blue";
    
}

//Fonction test3
function test3(){
    
      document.getElementById("content").className += "align-center";

}

//Fonction test4
function test4(){
    
    var newDiv = document.createElement("div");
    
    document.getElementById("content").innerHTML = "<div>Test 04</div>";
}

//Fonction test5
function test5(){

    var newDiv = document.createElement("div");

    newDiv.id = "bla";

    var newDiv = document.createElement("div");

    newDiv.id = "car";
}

//Function test6
//Inserer "bla" dans "car"
function test6(){
    
    var newDiv = document.createElement("div");

    document.getElementById("content").appendChild(newDiv);
}

// var x = document.getElementById("bla");
// var y = document.getElementById("car");

// x.appendChild(y);

function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 6
        });
        var infoWindow = new google.maps.InfoWindow({map: map});

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
      }

